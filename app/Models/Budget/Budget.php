<?php

namespace App\Models\Budget;

use App\Models\Scopes\BudgetScope;
use App\Models\Scopes\UserScope;
use App\Models\Transactions\Category;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Budget extends Model
{
    protected $fillable = ['category_id', 'amount', 'user_id', 'budget_date'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new UserScope);
        static::addGlobalScope(new BudgetScope);

        static::saving(function($budget) {
            $budget->user_id = $budget->user_id ?: auth()->id();
            $budget->budget_date = Carbon::parse($budget->budget_date)->toDateTimeString();
        });
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function balance()
    {
        return ($this->amount - $this->category->transactions->sum('amount'));
    }

    public function scopeByMonth($query, $month)
    {
        $query->where('budget_date', '>=', Carbon::parse("first day of {$month}"))
            ->where('budget_date', '<=', Carbon::parse("last day of {$month}"));
    }

    public function getMonth()
    {
        return (isset($this->budget_date)) ? Carbon::parse($this->budget_date)->format('M') : null;
    }
}
