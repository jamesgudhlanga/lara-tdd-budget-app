<?php

namespace App\Models\Transactions;

use App\Models\Scopes\TransactionScope;
use App\Models\Scopes\UserScope;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public $fillable = ['description', 'category_id', 'amount', 'user_id'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new UserScope);
        static::addGlobalScope(new TransactionScope);

        static::saving(function($transaction) {
            $transaction->user_id = $transaction->user_id ?: auth()->id();
        });
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function scopeByCategory($query, Category $category)
    {
        if($category->exists) {
            $query->where('category_id', $category->id);
        }
    }

    public function scopeByUser($query, User $user)
    {
        if($user->exists) {
            $query->where('user_id', $user->id);
        }
    }
}
