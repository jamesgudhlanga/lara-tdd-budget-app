<?php

namespace App\Http\Controllers\Transactions;

use App\Models\Transactions\Category;
use App\Models\Transactions\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransactionsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Category $category)
    {
        $transactionsQuery = Transaction::byCategory($category);
        $currentMonth = request('month') ?: Carbon::now()->format('M');
        $transactionsQuery->where('created_at', '>=', Carbon::parse('first day of '.$currentMonth))
            ->where('created_at', '<=', Carbon::parse('last day of '.$currentMonth));
        $transactions = $transactionsQuery->paginate();
        $months = ['Jan','Feb', 'Mar','Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        return view('transactions.index', compact('transactions', 'months', 'currentMonth'));
    }

    public function create()
    {
        $categories = Category::all();
        return view('transactions.create', compact('categories'));
    }

    public function store()
    {
        $this->validate(request(), [
            'description' => 'required', 'category_id' => 'required',
            'amount' => 'required|numeric'
        ]);
        Transaction::create(request()->all());
        return redirect('/transactions');
    }

    public function update(Transaction $transaction)
    {
        $this->validate(request(), [
            'description' => 'required', 'amount' => 'required|numeric', 'category_id' => 'required'
        ]);
        $transaction->update(request()->all());
        return redirect('/transactions');
    }

    public function edit(Transaction $transaction)
    {
        $categories = Category::all();
        return view('transactions.edit', compact('categories', 'transaction'));
    }

    public function destroy(Transaction $transaction)
    {
        $transaction->delete();
        return redirect('/transactions')->with(['success' => 'Transaction Successfully Deleted']);
    }
}
