<?php

namespace App\Http\Controllers\Categories;

use App\Models\Transactions\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{


    public function index()
    {
        $categories = Category::paginate();
        return view('categories.index', compact('categories'));
    }


    public function create()
    {

        return view('categories.create');
    }


    public function store(Request $request)
    {
        $this->validate(request(), [
            'name' => 'required'
        ]);
        Category::create(request()->all());
        return redirect('/categories')->with('success', 'Category Successfully Created');
    }


    public function edit(Category $category)
    {
        return view('categories.edit', compact('category'));
    }

    public function update(Category $category)
    {
        $this->validate(request(), ['name' => 'required']);
        $category->update(request()->all());
        return redirect(route('categories.index'));
    }


    public function destroy(Category $category)
    {
        $category->delete();
        return redirect(route('categories.index'));
    }
}
