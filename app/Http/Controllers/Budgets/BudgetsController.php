<?php

namespace App\Http\Controllers\Budgets;

use App\Models\Budget\Budget;
use App\Models\Transactions\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class BudgetsController extends Controller
{

    protected $months = ['Jan','Feb', 'Mar','Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    public function index()
    {
        $currentMonth = request('month') ?: Carbon::now()->format('M');
        $budgets = Budget::byMonth($currentMonth)->get();
        $months = $this->months;
        return view('budgets.index', compact('budgets', 'currentMonth', 'months'));
    }


    public function create()
    {
        $categories = Category::all();
        $months = $this->months;
        return view('budgets.create', compact('categories', 'months'));
    }


    public function store()
    {
        $this->validate(request(), [
            'budget_date'=>'required', 'category_id'=>'required', 'amount'=>'required']);
        Budget::create(request()->all());
        return redirect(route('budgets.index'));
    }


    public function update(Budget $budget)
    {
        $this->validate(request(), ['category_id' =>'required','budget_date'=>'required','amount'=>'required']);
        $budget->update(request()->all());
        return redirect(route('budgets.index'))->with('success', 'Budget successfully updated');
    }

    public function edit(Budget $budget)
    {
        $categories = Category::all();
        $months = $this->months;
        return view('budgets.edit', compact('budget', 'categories', 'months'));
    }

    public function destroy(Budget $budget)
    {
        $budget->delete();
        return redirect(route('budgets.index'))->with('success', 'Budget successfully deleted');
    }
}
