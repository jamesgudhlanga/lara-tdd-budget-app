<?php


Auth::routes();

Route::resource('transactions', 'Transactions\TransactionsController', ['except' => ['show']]);
Route::get('/transactions/{category?}', 'Transactions\TransactionsController@index');

Route::resource('categories', 'Categories\CategoriesController')->middleware('auth');

Route::resource('budgets', 'Budgets\BudgetsController')->middleware('auth');
