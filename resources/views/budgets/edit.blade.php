@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-8 mx-auto">
                <form action="{{route('budgets.update', $budget->id)}}" method="POST">
                    {{csrf_field()}}
                    {{method_field('PUT')}}
                    @include('budgets._partials.forms.budget', ['actionTitle' => 'Edit Budget', 'buttonTitle' => 'Update'])
                </form>
            </div>

        </div>
    </div>
@stop
