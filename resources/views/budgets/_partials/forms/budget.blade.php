<div class="card jeasy-card">
    <div class="card-body">
        <h3 class="card-title">{{ isset($actionTitle) ? $actionTitle : 'Create Budget' }}</h3>
        <div class="form-group {{ $errors->has('category_id') ? 'has-error' : '' }}">
            <label for="category_id">Category</label>
            <select name="category_id" id="category_id" class="form-control">
                <option value=""></option>
                @foreach($categories as $category)
                    <option value="{{ $category->id }}" {{ $category->id == (old('category_id') ?: isset($budget->category_id) ? $budget->category_id : '') ? 'selected' : ''}}>{{ $category->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group {{ $errors->has('amount') ? 'has-error' : '' }}">
            <label for="amount">Amount</label>
            <input type="number" name="amount" id="amount" class="form-control" value="{{ (old('amount') ?: isset($budget->amount) ? $budget->amount : '') }}">
        </div>
        <div class="form-group {{ $errors->has('budget_date') ? 'has-error' : '' }}">
            <label for="budget_date">Budget Date</label>
            <select name="budget_date" id="budget_date" class="form-control">
                <option value="">--choose one-</option>
                @foreach($months as $month)
                    <option value="{{$month}}" @if((isset($budget->budget_date)) && $month == $budget->getMonth()) selected @endif>{{$month}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="card-footer text-center">
        <button class="btn btn-primary" type="submit">{{ isset($buttonTitle) ? $buttonTitle : 'Save' }}</button>
    </div>
</div>
