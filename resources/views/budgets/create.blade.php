@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-8 mx-auto">
                <form action="{{route('budgets.store')}}" method="POST">
                    {{csrf_field()}}
                    @include('budgets._partials.forms.budget')
                </form>
            </div>

        </div>
    </div>
@stop
