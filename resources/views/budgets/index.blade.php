@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row m-2">
            <div class="col mb-2">
                <div class="float-left">
                    <h3>Monthly Budget</h3>
                </div>
                <div class="float-right">
                    <form method="GET" id="months-form">
                        <select name="month" id="month" class="form-control" onchange="this.form.submit()">
                            @foreach($months as $month)
                                <option value="{{$month}}" @if($currentMonth == $month) selected @endif>{{$month}}</option>
                            @endforeach
                        </select>
                    </form>
                </div>
            </div>
        </div>
        <div class="card jeasy-card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr><th>Category</th><th>Amount</th><th>Balance</th><th colspan="2">Action</th></tr>
                        </thead>
                        <tbody>
                        @foreach($budgets as $budget)
                            <tr>
                                <td>{{ $budget->category->name }}</td>
                                <td>{{ $budget->amount }}</td>
                                <td>{{ $budget->balance() }}</td>
                                <td>
                                    <a href="/budgets/{{ $budget->id }}/edit" class="btn btn-primary btn-xs">Edit</a>
                                </td>
                                <td>
                                    <form action="/budgets/{{ $budget->id }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button class="btn btn-danger btn-xs" type="submit">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

