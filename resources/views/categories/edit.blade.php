@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-8 mx-auto">
                <form action="{{ route('categories.update', [$category->slug]) }}" method="POST">
                    {{csrf_field()}}
                    {{ method_field('PUT') }}
                    @include('categories._partials.forms.category', ['actionTitle' => 'Edit Category', 'buttonTitle' => 'Update'])
                </form>
            </div>

        </div>
    </div>
@stop
