<div class="card jeasy-card">
    <div class="card-body">
        <h3 class="card-title">{{ isset($actionTitle) ? $actionTitle : 'Create Category' }}</h3>
        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
            <label for="name">Name</label>
            <input type="text" name="name" id="name" class="form-control" value="{{ (old('name') ?: isset($category->name) ? $category->name : '') }}">
        </div>
    </div>

    <div class="card-footer text-center">
        <button class="btn btn-primary" type="submit">{{ isset($buttonTitle) ? $buttonTitle : 'Save' }}</button>
    </div>
</div>
