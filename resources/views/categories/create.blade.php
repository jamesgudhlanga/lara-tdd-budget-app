@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-8 mx-auto">
                <form action="/categories" method="POST">
                    {{csrf_field()}}
                    @include('categories._partials.forms.category')
                </form>
            </div>

        </div>
    </div>
@stop
