@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card jeasy-card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr><th>Date</th><th>Name</th><th>Slug</th><th colspan="2">Action</th></tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td>{{ $category->created_at->format('m/d/Y') }}</td>
                                <td>{{ $category->name }}</td>
                                <td>{{ $category->slug }}</td>
                                <td>
                                    <a href="{{ route('categories.edit', [$category->slug]) }}" class="btn btn-primary btn-xs">Edit</a>
                                </td>
                                <td>
                                    <form action="{{ route('categories.destroy',[$category->slug]) }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button class="btn btn-danger btn-xs" type="submit">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $categories->links() }}
                </div>
            </div>
        </div>
    </div>
@stop

