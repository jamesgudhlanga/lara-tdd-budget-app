@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-8 mx-auto">
                <form action="/transactions/{{$transaction->id}}" method="POST">
                    {{csrf_field()}}
                    {{ method_field('PUT') }}
                    @include('transactions._partials.forms.transaction', ['buttonTitle' => 'Update', 'actionTitle' => 'Edit Transaction'])
                </form>
            </div>

        </div>
    </div>
@stop
