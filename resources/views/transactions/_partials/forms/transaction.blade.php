<div class="card jeasy-card">
    <div class="card-body">
        <h3 class="card-title">{{ isset($actionTitle) ? $actionTitle : 'Create Transaction' }}</h3>
        <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
            <label for="description">Description</label>
            <input type="text" name="description" id="description" class="form-control" value="{{ (old('description') ?: isset($transaction->description) ? $transaction->description : '') }}">
        </div>
        <div class="form-group {{ $errors->has('amount') ? 'has-error' : '' }}">
            <label for="amount">Amount</label>
            <input type="number" name="amount" id="amount" class="form-control" value="{{ (old('amount') ?: isset($transaction->amount) ? $transaction->amount : '') }}">
        </div>
        <div class="form-group {{ $errors->has('category_id') ? 'has-error' : '' }}">
            <label for="category_id">Category</label>
            <select name="category_id" id="category_id" class="form-control">
                <option value=""></option>
                @foreach($categories as $category)
                    <option value="{{ $category->id }}" {{ $category->id == (old('category_id') ?: isset($transaction->category_id) ? $transaction->category_id : '') ? 'selected' : ''}}>{{ $category->name }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="card-footer text-center">
        <button class="btn btn-primary" type="submit">{{ isset($buttonTitle) ? $buttonTitle : 'Save' }}</button>
    </div>
</div>
