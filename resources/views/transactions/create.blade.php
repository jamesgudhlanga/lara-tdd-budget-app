@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-8 mx-auto">
                <form action="/transactions" method="POST">
                    {{csrf_field()}}
                    @include('transactions._partials.forms.transaction')
                </form>
            </div>

        </div>
    </div>
@stop
