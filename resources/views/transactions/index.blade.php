@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row m-2">
            <div class="col mb-2">
                <div class="float-right">
                    <form method="GET" id="months-form">
                        <select name="month" id="month" class="form-control" onchange="this.form.submit()">
                            @foreach($months as $month)
                                <option value="{{$month}}" @if($currentMonth == $month) selected @endif>{{$month}}</option>
                            @endforeach
                        </select>
                    </form>
                </div>
            </div>
        </div>
        <div class="card jeasy-card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr><th>Date</th><th>Description</th><th>Category</th><th>Amount</th><th colspan="2">Action</th></tr>
                        </thead>
                        <tbody>
                            @foreach($transactions as $transaction)
                                <tr>
                                    <td>{{ $transaction->created_at->format('m/d/Y') }}</td>
                                    <td>{{ $transaction->description }}</td>
                                    <td>{{ $transaction->category->name }}</td>
                                    <td>{{ $transaction->amount }}</td>
                                    <td>
                                        <a href="/transactions/{{ $transaction->id }}/edit" class="btn btn-primary btn-xs">Edit</a>
                                    </td>
                                    <td>
                                        <form action="/transactions/{{ $transaction->id }}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button class="btn btn-danger btn-xs" type="submit">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $transactions->links() }}
                </div>
            </div>
        </div>
</div>
@stop

