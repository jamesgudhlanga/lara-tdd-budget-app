<?php

namespace Tests\Feature\Transactions;

use App\Models\Transactions\Category;
use App\Models\Transactions\Transaction;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateTransactionTest extends TestCase
{

    use RefreshDatabase;

    /**
     * @test
     */
    public function it_can_update_transaction()
    {
        $category = $this->create(Category::class);
        $transaction = $this->create(Transaction::class);
        $newTransaction = $this->make(Transaction::class, ['category_id' => $category->id]);

        $this->put("/transactions/{$transaction->id}", $newTransaction->toArray())
            ->assertRedirect('/transactions');

        $this->get('/transactions')
            ->assertSee($newTransaction->description);
    }

    /**
     * @test
     */
    public function that_description_is_required_for_updating_transaction()
    {
        $this->putTransaction(['description' => null])
            ->assertStatus(302)
            ->assertSessionHasErrors('description');

    }

    /**
     * @test
     */
    public function that_amount_is_required_for_updating_transaction()
    {
        $this->putTransaction(['amount' => null])
            ->assertStatus(302)
            ->assertSessionHasErrors('amount');
    }

    /**
     * @test
     */
    public function that_amount_is_numeric_only_for_updating_transaction()
    {
        $this->putTransaction(['amount' => 'abc'])
            ->assertStatus(302)
            ->assertSessionHasErrors('amount');
    }

    /**
     * @test
     */
    public function that_category_is_required_for_updating_transaction()
    {
        $this->putTransaction(['category_id' => null])
            ->assertStatus(302)
            ->assertSessionHasErrors('category_id');
    }

    public function putTransaction ($overrides = [])
    {
        $transaction = $this->create(Transaction::class);
        $newTransaction = $this->make(Transaction::class, $overrides);
        return $this->withExceptionHandling()->put("/transactions/{$transaction->id}", $newTransaction->toArray());
    }

    /**
     * @test
     */
    public function it_can_edit_transaction ()
    {
        $transaction = $this->create(Transaction::class);
        $this->get("/transactions/{$transaction->id}/edit")
            ->assertViewHas('categories')
            ->assertStatus(200)
            ->assertSee($transaction->description);
    }

}
