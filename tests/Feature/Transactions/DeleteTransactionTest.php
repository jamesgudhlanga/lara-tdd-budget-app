<?php

namespace Tests\Feature\Transactions;

use App\Models\Transactions\Transaction;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeleteTransactionTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_can_delete_a_transaction()
    {
        $transaction = $this->create(Transaction::class);

        $this->delete("/transactions/{$transaction->id}")
            ->assertRedirect('/transactions')
            ->assertSessionHas('success');

        $this->assertDatabaseMissing('transactions', $transaction->toArray());
    }
}
