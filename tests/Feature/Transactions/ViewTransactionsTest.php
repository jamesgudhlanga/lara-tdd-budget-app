<?php

namespace Tests\Feature\Transactions;

use App\Models\Transactions\Category;
use App\Models\Transactions\Transaction;
use App\Models\Users\User;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ViewTransactionsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_allows_only_authenticated_users_to_transactions_list ()
    {
        $this->signOut()
            ->withExceptionHandling()
            ->get('/transactions')
            ->assertRedirect('/login');
    }

    /**
     * @test
     */
    public function it_only_displays_transactions_that_belongs_to_currently_logged_in_user()
    {
        $category = $this->create(Category::class);
        $otherUser = create(User::class);
        $transaction = create(Transaction::class, ['user_id' => $this->user->id, 'category_id' => $category->id]);
        $otherTransaction = create(Transaction::class, ['user_id' => $otherUser->id]);

        $this->get('/transactions')
            ->assertSee($transaction->description)
            ->assertDontSee($otherTransaction->description);
    }

    /**
     * @test
     */
    public function it_can_display_all_transactions()
    {
        $category = $this->create(Category::class);
        $transaction = $this->create(Transaction::class, ['category_id' => $category->id]);
        $this->get('/transactions')
            ->assertSee($transaction->description)
            ->assertSee($transaction->category->name)
            ->assertSee('Edit')
            ->assertSee('Delete');
    }

    /**
     * @test
     */
    public function it_can_filter_transactions_by_category()
    {
        $category = $this->create(Category::class);

        $transaction = $this->create(Transaction::class, ['category_id' => $category->id]);
        $otherTransaction = $this->create(Transaction::class);

        $this->get('/transactions/'.$category->slug)
            ->assertSee($transaction->description)
            ->assertDontSee($otherTransaction->description);
    }

    /**
     * @test
     */
    public function it_can_filter_transactions_by_month()
    {
        $category = $this->create(Category::class);
        $currentTransaction = $this->create(Transaction::class);
        $pastTransaction = $this->create(Transaction::class,
            ['created_at' => Carbon::now()->subMonth(2), 'category_id' => $category->id]);

        $this->get('/transactions?month='.Carbon::now()->subMonth(2)->format('M'))
            ->assertSee($pastTransaction->description)
            ->assertDontSee($currentTransaction->description);
    }

    /**
     * @test
     */
    public function it_can_filter_transactions_by_current_month_by_default()
    {
        $category = $this->create(Category::class);
        $currentTransaction = $this->create(Transaction::class, ['category_id' => $category->id]);
        $pastTransaction = $this->create(Transaction::class,
            ['created_at' => Carbon::now()->subMonth(2)]);

        $this->get('/transactions')
            ->assertSee($currentTransaction->description)
            ->assertDontSee($pastTransaction->description);
    }
}
