<?php

namespace Tests\Feature\Transactions;

use App\Models\Transactions\Category;
use App\Models\Transactions\Transaction;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateTransactionsTest extends TestCase
{
   use RefreshDatabase;

    /**
     * @test
     */
   public function it_can_store_transactions()
   {
       $category = $this->create(Category::class);
       $transaction = $this->make(Transaction::class, ['category_id' => $category->id]);

       $this->post(route('transactions.store', $transaction->toArray()))
            ->assertRedirect('/transactions');

       $this->get('/transactions')
           ->assertSee($transaction->description);
   }

    /**
     * @test
     */
   public function it_can_not_store_transactions_without_a_description()
   {
       $this->postTransaction(['description' => null])->assertSessionHasErrors('description');
   }

    /**
     * @test
     */
    public function it_can_not_store_transactions_without_a_category()
    {
        $this->postTransaction(['category_id' => null])->assertSessionHasErrors('category_id');
    }

    /**
     * @test
     */
    public function it_can_not_store_transactions_without_an_amount()
    {
        $this->postTransaction(['amount' => null])->assertSessionHasErrors('amount');
    }

    /**
     * @test
     */
    public function it_can_not_store_transactions_without_a_valid_amount()
    {
        $this->postTransaction(['amount' => 'abc'])->assertSessionHasErrors('amount');
    }

    public function postTransaction ($overrides = [])
    {
        $transaction = make(Transaction::class, $overrides);
        return $this->withExceptionHandling()->post('/transactions', $transaction->toArray());
    }

    /**
     * @test
     */
    public function it_can_create_transaction ()
    {
        $this->get('/transactions/create')
            ->assertStatus(200)
            ->assertViewHas('categories')
            ->assertSee('Save');
    }
}
