<?php

namespace Tests\Feature\Categories;

use App\Models\Transactions\Category;
use App\Models\Users\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ViewCategoriesTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_allows_only_authenticated_users_to_categories_list ()
    {
        $this->signOut()
            ->withExceptionHandling()
            ->get('/categories')
            ->assertRedirect('/login');
    }

    /**
     * @test
     */
    public function it_only_displays_categories_that_belongs_to_currently_logged_in_user()
    {
        $otherUser = create(User::class);
        $category = $this->create(Category::class,['user_id' => $this->user->id]);
        $otherCategory = create(Category::class, ['user_id' => $otherUser->id]);

        $this->get('/categories')
            ->assertSee($category->name)
            ->assertDontSee($otherCategory->name);
    }

    /**
     * @test
     */
   public function can_view_all_categories()
   {
       $category = $this->create(Category::class);
       $this->get('/categories')
           ->assertSee($category->name);
       $this->assertDatabaseHas('categories', $category->toArray());
   }
}
