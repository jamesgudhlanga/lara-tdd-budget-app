<?php

namespace Tests\Feature\Categories;

use App\Models\Transactions\Category;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeleteCategoryTest extends TestCase
{

    use RefreshDatabase;

    /**
     * @test
     */
    public function it_can_delete_a_category()
    {
        $category = $this->create(Category::class);
        $this->delete(route('categories.destroy', [$category->slug]))
            ->assertRedirect(route('categories.index'));

        $this->assertDatabaseMissing('categories', $category->toArray());
    }
}
