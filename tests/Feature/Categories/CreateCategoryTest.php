<?php

namespace Tests\Feature\Categories;

use App\Models\Transactions\Category;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateCategoryTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_can_create_a_category()
    {
        $this->get('/categories/create')
            ->assertStatus(200)
            ->assertSee('Save')
            ->assertSee('Create Category');
    }

    /**
     * @test
     */
    public function it_can_store_a_category()
    {
        $category = $this->make(Category::class);

        $this->post(route('categories.store', $category->toArray()))
            ->assertRedirect(route('categories.index'))
            ->assertSessionHas('success');
        $this->assertDatabaseHas('categories', $category->toArray());

        $this->get(route('categories.index'))
            ->assertSee($category->name);
    }

    /**
     * @test
     */
    public function that_name_is_required_to_store_category()
    {
        $category = $this->make(Category::class, ['name' => null]);
        $this->withExceptionHandling()
            ->post(route('categories.store', $category->toArray()))
            ->assertSessionHasErrors('name');
        $this->assertDatabaseMissing('categories', $category->toArray());
    }

}
