<?php

namespace Tests\Feature\Categories;

use App\Models\Transactions\Category;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateCategoriesTest extends TestCase
{
   use RefreshDatabase;

    /**
     * @test
     */
    public function it_can_update_category()
    {
        $category = $this->create(Category::class);
        $newCategory = $this->make(Category::class);

        $this->put("/categories/{$category->slug}", $newCategory->toArray())
            ->assertRedirect('/categories');

        $this->get('/categories')
            ->assertSee($newCategory->name);
    }

    /**
     * @test
     */
    public function that_name_is_required_for_updating_category()
    {
        $category = $this->create(Category::class);
        $newCategory = $this->make(Category::class, ['name' => null]);

        $this->withExceptionHandling()->put("/categories/{$category->slug}", $newCategory->toArray())
            ->assertSessionHasErrors('name');
    }

    /**
     * @test
     */
    public function it_can_edit_a_category()
    {
        $category = $this->create(Category::class);

        $this->get("/categories/{$category->slug}/edit")
            ->assertSee($category->name);
    }
}
