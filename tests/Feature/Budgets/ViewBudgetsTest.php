<?php

namespace Tests\Feature\Budgets;

use App\Models\Budget\Budget;
use App\Models\Transactions\Category;
use App\Models\Users\User;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ViewBudgetsTest extends TestCase
{
   use RefreshDatabase;

    /**
     * @test
     */
    public function it_allows_only_authenticated_users_to_budgets_list ()
    {
        $this->signOut()
            ->withExceptionHandling()
            ->get(route('budgets.index'))
            ->assertRedirect('/login');
    }

    /**
     * @test
     */
   public function it_should_display_budget_for_the_current_month_by_default()
   {
       $category= $this->create(Category::class);
       $thisMonthBudget = $this->create(Budget::class, ['category_id' => $category->id]);
       $lastMonthBudget = $this->create(Budget::class, ['category_id' => $category->id, 'budget_date' => Carbon::now()->subMonth()]);

       $this->get(route('budgets.index'))
           ->assertSee((string) $thisMonthBudget->amount)
           ->assertSee((string) $thisMonthBudget->balance())
           ->assertDontSee((string) $lastMonthBudget->amount)
           ->assertDontSee((string) $lastMonthBudget->balance());
   }

    /**
     * @test
     */
    public function it_only_displays_budgets_that_belongs_to_currently_logged_in_user()
    {
        $category = $this->create(Category::class);
        $otherUser = create(User::class);
        $budget = create(Budget::class, ['user_id' => $this->user->id, 'category_id' => $category->id]);
        $otherBudget = create(Budget::class, ['user_id' => $otherUser->id]);

        $this->get('/budgets')
            ->assertSee((string) $budget->amount)
            ->assertDontSee((string) $otherBudget->amount);
    }
}
