<?php

namespace Tests\Feature\Budgets;

use App\Models\Budget\Budget;
use App\Models\Transactions\Category;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateBudgetTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_can_store_a_budget()
    {
        $category = $this->create(Category::class);
        $budget = $this->make(Budget::class, ['category_id' => $category->id]);

        $this->post('/budgets', $budget->toArray())
            ->assertRedirect(route('budgets.index'));

        $this->get(route('budgets.index'))
        ->assertSee((string)$budget->amount);
    }

    /**
     * @test
     */
    public function that_budget_date_is_a_required_field ()
    {
        $this->postBudget(['budget_date' => null])->assertSessionHasErrors('budget_date');
    }

    /**
     * @test
     */
    public function that_category_is_a_required_field ()
    {
        $this->postBudget(['category_id' => null])->assertSessionHasErrors('category_id');
    }

    /**
     * @test
     */
    public function that_amount_is_a_required_field ()
    {
        $this->postBudget(['amount' => null])->assertSessionHasErrors('amount');
    }

    public function postBudget($overrides = [])
    {
        $budget = $this->make(Budget::class, $overrides);
        return $this->withExceptionHandling()->post('/budgets', $budget->toArray());
    }

    /**
     * @test
     */
    public function it_can_create_a_budget ()
    {
        $this->get(route('budgets.create'))
            ->assertViewHas('categories')
            ->assertStatus(200);
    }
}
