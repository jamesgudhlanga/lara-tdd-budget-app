<?php

namespace Tests\Feature\Budgets;

use App\Models\Budget\Budget;
use App\Models\Transactions\Category;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateBudgetTest extends TestCase
{

    use RefreshDatabase;

    /**
     * @test
     */
   public function it_can_update_budget()
   {
        $category = $this->create(Category::class);

        $budget = $this->create(Budget::class, ['category_id' => $category->id]);
        $budgetUpdate = $this->make(Budget::class);

        $this->put("budgets/{$budget->id}", $budgetUpdate->toArray())
            ->assertRedirect(route('budgets.index'))
            ->assertSessionHas('success');

        $this->assertDatabaseHas('budgets', ['amount' => $budgetUpdate->amount]);
   }

    /**
     * @test
     */
   public function that_category_id_is_required_to_update_budget()
   {

       $this->updateBudget(['category_id' => null])->assertSessionHasErrors('category_id');

   }

    /**
     * @test
     */
    public function that_budget_date_is_required_to_update_budget()
    {
        $this->updateBudget(['budget_date' => null])->assertSessionHasErrors('budget_date');

    }

    /**
     * @test
     */
    public function that_amount_is_required_to_update_budget()
    {
        $this->updateBudget(['amount' => null])->assertSessionHasErrors('amount');

    }

    private function updateBudget($overrides =[])
    {
        $category = $this->create(Category::class);

        $budget = $this->create(Budget::class, ['category_id' => $category->id]);
        $budgetUpdate = $this->make(Budget::class, $overrides);

        return $this->withExceptionHandling()->put("budgets/{$budget->id}", $budgetUpdate->toArray());
    }

    /**
     * @test
     */
    public function it_can_edit_budget()
    {
        $category = $this->create(Category::class);
        $budget = $this->create(Budget::class, ['category_id' => $category->id]);

        $this->get("/budgets/{$budget->id}/edit")
            ->assertViewHas('categories')
            ->assertSee((string) $budget->amount);
    }
}
