<?php

namespace Tests\Feature\Budgets;

use App\Models\Budget\Budget;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeleteBudgetTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_can_delete_a_budget()
    {
        $budget = $this->create(Budget::class);

        $this->delete("/budgets/{$budget->id}")
            ->assertRedirect(route('budgets.index'))
            ->assertSessionHas('success');

        $this->assertDatabaseMissing('budgets', $budget->toArray());
    }
}
