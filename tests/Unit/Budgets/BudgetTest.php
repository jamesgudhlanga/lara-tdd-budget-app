<?php

namespace Tests\Unit\Budgets;

use App\Models\Budget\Budget;
use App\Models\Transactions\Category;
use App\Models\Transactions\Transaction;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BudgetTest extends TestCase
{

    use RefreshDatabase;
    /**
     * @test
     */
    public function it_has_a_balance()
    {
        $category = $this->create(Category::class);
        $transactions = $this->create(Transaction::class, ['category_id' => $category->id], 3);
        $budget = $this->create(Budget::class, ['category_id' => $category->id]);

        $expectedBalance = ($budget->amount - $transactions->sum('amount'));

        $this->assertEquals($expectedBalance, $budget->balance());
    }
}
