<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Budget\Budget::class, function (Faker $faker) {
    return [
        'amount' => $faker->randomFloat(2, 500, 1000),
        'budget_date' => \Carbon\Carbon::now()->format('M'),
        'category_id' => function ()
        {
            return create(\App\Models\Transactions\Category::class)->id;
        },
        'user_id' => function ()
        {
            return create(\App\Models\Users\User::class)->id;
        }
    ];
});
