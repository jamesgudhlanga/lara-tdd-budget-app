<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Transactions\Category::class, function (Faker $faker) {
    $name = $faker->word;
    return [
        'name' => $name,
        'slug' => str_slug($name),
        'user_id' => function ()
        {
            return create(App\Models\Users\User::class)->id;
        }
    ];
});
