<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Transactions\Transaction::class, function (Faker $faker) {
    return [
        'description' => $faker->sentence(2),
        'amount' => $faker->numberBetween(5, 10) ,
        'category_id' => function(){
            return create(\App\Models\Transactions\Category::class)->id;
        },
        'user_id' => function ()
        {
            return create(App\Models\Users\User::class)->id;
        }
    ];
});
